extends Control

export var winning_points : int = 150
export var menu : PackedScene 

var points : int = 888
var textura : Control

func _ready() -> void:
	if Globals.points >= 0 :
		points = Globals.points
	$Labels/Puntaje.text = str(points)
	
	if points >= winning_points:
		$Fondo.texture = load("res://EndScreen/index.jpg")
	else:
		$Fondo.texture = load("res://EndScreen/index.jpg")

	$AnimationPlayer.play("ganar")

func _on_AnimationPlayer_animation_finished(anim_name: String) -> void:
	get_tree().change_scene_to(menu)
