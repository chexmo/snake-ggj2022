extends Node

const INVALID_INDEX = -1

const CELL_X_SIZE = 120 # reference for map creation and units (snake, mouse, etc) movement
const CELL_Y_SIZE = 120 # reference for map creation and units (snake, mouse, etc) movement
const CELL_SIZE_VECTOR = Vector2(CELL_X_SIZE, CELL_Y_SIZE)

enum MapUnits{
	NO_TRANSITABLE = 0,
	TRANSITABLE = 1,
	SNAKE_BODY = 2,
	SNAKE_TAIL = 3,
	RIGHT_SNAKE_HEAD = 4,
	LEFT_SNAKE_HEAD = 5,
	UP_SNAKE_HEAD = 6,
	DOWN_SNAKE_HEAD = 7,
	EMPTY = 8,
	MAP_ENTRANCE = 9,
	HARD_NO_TRANSITABLE = 10,
	SPAWN_FLOOR = 11,
	OBSTACLE = 12,
	FOOD = 13,
}

func _ready():
	randomize()


static func isSnakeCell(cell:int)->bool:
	match cell:
		MapUnits.SNAKE_BODY, \
		MapUnits.SNAKE_TAIL:
			return true
	
	return isSnakeHeadCell(cell)

static func isSnakeHeadCell(cell:int)->bool:
	match cell:
		MapUnits.RIGHT_SNAKE_HEAD, \
		MapUnits.LEFT_SNAKE_HEAD, \
		MapUnits.UP_SNAKE_HEAD, \
		MapUnits.DOWN_SNAKE_HEAD:
			return true
	
	return false


static func isTransitableOrSnakeCell(cell:int)->bool:
	match cell:
		MapUnits.TRANSITABLE, \
		MapUnits.MAP_ENTRANCE, \
		MapUnits.SPAWN_FLOOR:
			return true
	
	return isSnakeCell(cell)

static func isNoTransitableCell(cell:int)->bool:
	match cell:
		MapUnits.NO_TRANSITABLE, \
		MapUnits.HARD_NO_TRANSITABLE:
			return true
	
	return false

static func isSpawnRelatedCell(cell:int)->bool:
	if(isSnakeCell(cell)):
		return true
	
	match cell:
		MapUnits.SPAWN_FLOOR, \
		MapUnits.MAP_ENTRANCE:
			return true
	
	return false

static func position2Grid(position:Vector2)->Array:
	# it has to ensure integer division
	return [int(position.y) / int(CELL_Y_SIZE), int(position.x) / int(CELL_X_SIZE)]

static func obtainSnakeHeadCellDirection(cell:int)->Vector2:
	var auxDir:Vector2 = Vector2.ZERO
	
	match(cell):
		MapUnits.RIGHT_SNAKE_HEAD:
			auxDir = Vector2.RIGHT
		MapUnits.LEFT_SNAKE_HEAD:
			auxDir = Vector2.LEFT
		MapUnits.UP_SNAKE_HEAD:
			auxDir = Vector2.UP
		MapUnits.DOWN_SNAKE_HEAD:
			auxDir = Vector2.DOWN
	
	return auxDir

