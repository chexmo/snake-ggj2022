extends Node

signal happy_started

signal evil_started

func emit_happy_started():
	emit_signal("happy_started")

func emit_evil_started():
	emit_signal("evil_started")
