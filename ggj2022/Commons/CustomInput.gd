extends Node

var last_input : Vector2 = Vector2.RIGHT

onready var menu_scene = preload("res://Menu/Menu.tscn")

var dirs = {
	"move_up": Vector2.UP,
	"move_down": Vector2.DOWN,
	"move_left": Vector2.LEFT,
	"move_right": Vector2.RIGHT
}

func _process(_delta: float) -> void:

	if Input.is_action_just_pressed("quit"):
		get_tree().change_scene_to(menu_scene)

	for d in dirs.keys():
		if Input.is_action_pressed(d):
			last_input = dirs.get(d)
