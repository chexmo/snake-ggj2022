extends Node2D

export(float,-80,0) var high_volume = -8
export(float,-80,0) var low_volume = -80

onready var gamePlayMapGenerator:Node2D = $GamePlayMapGenerator
onready var snake:Node2D = $Snake
onready var mapCells:Node2D = $MapCells
onready var obstacles:Node2D = $Obstacles
onready var food:Node2D = $Food

onready var happyPlayer : AudioStreamPlayer = $MusicGood
onready var evilPlayer : AudioStreamPlayer = $MusicBad

func _ready() -> void:
	Globals.points = 0
	
	snake.generateInitialSnake()
	gamePlayMapGenerator.startMapGeneration(snake, mapCells, obstacles, food)
	
	EventManager.connect("evil_started", self, "_on_evil_started")
	EventManager.connect("happy_started", self, "_on_happy_started")
	
	_on_happy_started()


func _on_happy_started():
	play_music()
	happyPlayer.volume_db = high_volume
	evilPlayer.volume_db = low_volume


func _on_evil_started():
	play_music()
	happyPlayer.volume_db = low_volume
	evilPlayer.volume_db = high_volume

func play_music():
	if not happyPlayer.playing:
		happyPlayer.play()
	if not evilPlayer.playing:
		evilPlayer.play()
