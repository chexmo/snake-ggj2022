extends Node2D
class_name Head

signal entered(area)

var direction : Vector2 = Vector2.RIGHT
var old_position : Vector2 = Vector2.ZERO
var current_position : Vector2 = Vector2.ZERO

onready var snake = get_parent()
onready var animatedSprite:AnimatedSprite = $AnimatedSprite

var newLevelJustLoaded:bool = false

func cacheReferences() -> void:
	animatedSprite = $AnimatedSprite

func move(diff: Vector2) -> void:
	if diff + direction * MapReferences.CELL_SIZE_VECTOR == Vector2.ZERO : diff *= -1
	old_position = current_position
	current_position = (global_position + diff).snapped(MapReferences.CELL_SIZE_VECTOR)
	direction = diff.normalized()
	
	global_position = current_position


func setDirection(dir:Vector2)->void:
	direction = dir
	CustomInput.last_input = direction # updates next movement

# updates current position based on current global_position
func updateCurrentPos()->void:
	current_position = global_position


func refresh_sprites() -> void:
	match direction:
		Vector2.RIGHT: animatedSprite.animation = "head_right"
		Vector2.DOWN: animatedSprite.animation = "head_down"
		Vector2.LEFT: animatedSprite.animation = "head_left"
		Vector2.UP: animatedSprite.animation = "head_up"
		_: animatedSprite.animation = "head_right"


func _on_Area2D_area_entered(area: Area2D) -> void:
	emit_signal("entered",area)
