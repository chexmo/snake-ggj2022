extends Node2D
class_name Tail

var direction : Vector2 = Vector2.RIGHT
var old_position : Vector2 = Vector2.ZERO
var current_position : Vector2 = Vector2.ZERO

onready var animatedSprite:AnimatedSprite = $AnimatedSprite

func cacheReferences() -> void:
	animatedSprite = $AnimatedSprite

func move(target : Vector2, next_target: Vector2) -> void:
	old_position = current_position
	current_position = target
	direction = next_target - target
	
	global_position = current_position

func updateCurrentPos()->void:
	current_position = global_position

func refresh_sprites() -> void:
	match direction.normalized():
		Vector2.RIGHT: animatedSprite.animation = "tail_right"
		Vector2.DOWN: animatedSprite.animation = "tail_down"
		Vector2.LEFT: animatedSprite.animation = "tail_left"
		Vector2.UP: animatedSprite.animation = "tail_up"
		_: animatedSprite.animation = "tail_right"
