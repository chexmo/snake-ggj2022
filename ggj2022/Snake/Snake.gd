extends Node2D

signal finish_game
const MapMatrixBlockGroupGenerator = preload("res://Map/MapMatrix/MapMatrixBlockGroupGenerator.gd")
const BodyPieceInstancer = preload("res://Snake/Body/BodyPiece.tscn")

export(int, 1, 10) var cellsPerMovement : int = 1
export(float, 10) var movementCDSeconds : float = 0.3
export(float, 5) var spawnStunnedSeconds: float = 2

const INITIAL_LEFT2RIGHT_SNAKE_MATRIX = [[3,2,2,2,2,2,2,2,2,2,2,2,2,2,4]] # it has to be a left to right snake in a single row

onready var head:Node2D = $Head
onready var body:Node2D = $Body
onready var tail:Node2D = $Tail
onready var audioStreamPlayer:AudioStreamPlayer = $AudioStreamPlayer
onready var stoppedSnakeTimer:Timer = $StoppedSnakeTimer

var input : Vector2
var canMove:bool
var snakeMatrix:Array
var currentMovementCDSeconds:float

func _ready() -> void:
	stoppedSnakeTimer.one_shot = true
	stoppedSnakeTimer.connect("timeout", self, "startMovement")
	stoppedSnakeTimer.set_wait_time(1)
	input = Vector2.ZERO 
	canMove = true
	currentMovementCDSeconds = 0

func cacheReferences():
	if head: # references are already cached
		return
	
	head = $Head
	body = $Body
	tail = $Tail
	audioStreamPlayer = $AudioStreamPlayer
	
	head.cacheReferences()
	tail.cacheReferences()
	for bodyPiece in body.get_children():
		bodyPiece.cacheReferences()



func _physics_process(delta: float) -> void:
	currentMovementCDSeconds += delta
	if canMove and currentMovementCDSeconds >= movementCDSeconds:
		currentMovementCDSeconds = 0 # resets time wait for the next movement
		for i in range(cellsPerMovement):
			move(delta)

func move(delta: float):
	var fixedLastInput:Vector2 = Vector2(CustomInput.last_input.x * MapReferences.CELL_X_SIZE, CustomInput.last_input.y * MapReferences.CELL_Y_SIZE)
	head.move(fixedLastInput)
	
	var target:Vector2 = head.old_position
	var next_target:Vector2 = head.current_position
	
	for auxSnakeBodyPiece in body.get_children(): 
		# it will take some time for the body to correct itself by movement
		# since cells are not placed in order to the body
		auxSnakeBodyPiece.move(target, next_target)
		target = auxSnakeBodyPiece.old_position
		next_target = auxSnakeBodyPiece.current_position
	
	tail.move(target, next_target)
	
	refresh_sprites()


func stopMovement(seconds:float)->void:
	canMove = false
	currentMovementCDSeconds = 0 # it will have to start spending time to move from the beginning
	stoppedSnakeTimer.set_wait_time(seconds)
	stoppedSnakeTimer.start() # it will start movement again

func startMovement()->void:
	canMove = true

func refresh_sprites() -> void:
	head.refresh_sprites()
	for b in body.get_children():
		if b.has_method("refresh_sprites"): 
			b.refresh_sprites()
	tail.refresh_sprites()


func eat():
	audioStreamPlayer.play_eat()
	pass


func receive_damage():
	audioStreamPlayer.play_damage()
	pass


func generateInitialSnake()->void:
	snakeMatrix = INITIAL_LEFT2RIGHT_SNAKE_MATRIX
	generateLeftToRightSnake()


func updatePositions(diffWithLastPosition:Array)->void:
	var positionOffset:Vector2 = Vector2.ZERO
	if(diffWithLastPosition != []):
		positionOffset = Vector2(diffWithLastPosition[1] * MapReferences.CELL_X_SIZE, diffWithLastPosition[0] * MapReferences.CELL_Y_SIZE)
	addOffsetToPositions(positionOffset)
	stopMovement(spawnStunnedSeconds)


# snakeMatrix should be previously updated
# initial snake should be a left to right single rowed snake (eg [[3,2,2,2,2,2,4], [3,2,2,2,4], ...])
func generateLeftToRightSnake()->void:
	cacheReferences()
	
	var i:int = 0 # single row
	var n:int = snakeMatrix.size()
	var m:int = snakeMatrix[0].size()
	var currentBodyPieceIndex:int = 1
	var auxSnakePiece:Node2D
	var isSnakeCell:bool
	var isSnakeHeadCell:bool
	for j in range(m - 1, -1, -1): # right to left in order to properly place the body pieces
		isSnakeCell = false
		isSnakeHeadCell = false
		match(snakeMatrix[i][j]):
			MapReferences.MapUnits.SNAKE_BODY:
				isSnakeCell = true
				auxSnakePiece = body.get_node("BodyPiece" + str(currentBodyPieceIndex))
				if(!auxSnakePiece):
					auxSnakePiece = BodyPieceInstancer.instance()
					auxSnakePiece.name = "BodyPiece" + str(currentBodyPieceIndex)
					body.add_child(auxSnakePiece)
				currentBodyPieceIndex += 1
			MapReferences.MapUnits.SNAKE_TAIL:
				isSnakeCell = true
				auxSnakePiece = tail
			_:
				if(MapReferences.isSnakeHeadCell(snakeMatrix[i][j])):
					isSnakeCell = true
					isSnakeHeadCell = true
					auxSnakePiece = head
		
		if(isSnakeCell):
			auxSnakePiece.position = Vector2(j * MapReferences.CELL_X_SIZE, i * MapReferences.CELL_Y_SIZE)
		if(isSnakeHeadCell):
			head.setDirection(MapReferences.obtainSnakeHeadCellDirection(snakeMatrix[i][j]))
	
	# erase remaining snake pieces since they were not used
	auxSnakePiece = get_node("Body/BodyPiece" + str(currentBodyPieceIndex))
	while(auxSnakePiece):
		auxSnakePiece.queue_free()
		currentBodyPieceIndex += 1
		auxSnakePiece = get_node("Body/BodyPiece" + str(currentBodyPieceIndex))


func addOffsetToPositions(offset:Vector2)->void:
	tail.position += offset
	tail.updateCurrentPos() # this is important in order to start movement properly
	head.position += offset
	head.updateCurrentPos() # this is important in order to start movement properly
	for bodyPiece in body.get_children():
		bodyPiece.position += offset
		bodyPiece.updateCurrentPos() # this is important in order to start movement properly
	
	


func snake2Matrix()->Array:
	var tailCellPos:Array = MapReferences.position2Grid(tail.position)
	var headCellPos:Array = MapReferences.position2Grid(head.position)
	var bodyCellPositions:Array = []
	var bodyPieces:Array = body.get_children()
	bodyCellPositions.resize(bodyPieces.size())
	for i in range(bodyPieces.size()):
		bodyCellPositions[i] = MapReferences.position2Grid(bodyPieces[i].position)
	
	var maxI:int = -9999999
	var minI:int = 9999999
	var maxJ:int = -9999999
	var minJ:int = 9999999
	var auxCellPositions:Array = bodyCellPositions + [tailCellPos] + [headCellPos]
	for auxCellPosition in auxCellPositions:
		if(auxCellPosition[0] > maxI):
			maxI = auxCellPosition[0]
		if(auxCellPosition[0] < minI):
			minI = auxCellPosition[0]
		if(auxCellPosition[1] > maxJ):
			maxJ = auxCellPosition[1]
		if(auxCellPosition[1] < minJ):
			minJ = auxCellPosition[1]
	
	var n:int = maxI - minI + 1
	var m:int = maxJ - minJ + 1
	var auxSnakeMat:Array = []
	
	auxSnakeMat = MapMatrixBlockGroupGenerator.matrixOfSameValues(n, m, MapReferences.MapUnits.NO_TRANSITABLE)
	# defaults every cell to prevent null pointer exceptions
	
	var nCorrection:int = maxI - n + 1
	var mCorrection:int = maxJ - m + 1
	
	auxSnakeMat[tailCellPos[0] - nCorrection][tailCellPos[1] - mCorrection] = MapReferences.MapUnits.SNAKE_TAIL
	var normalizedHeadDir:Vector2 = head.direction.normalized()
	var auxHeadCellUnit:int
	match(normalizedHeadDir):
		Vector2.RIGHT:
			auxHeadCellUnit = MapReferences.MapUnits.RIGHT_SNAKE_HEAD
		Vector2.LEFT:
			auxHeadCellUnit = MapReferences.MapUnits.LEFT_SNAKE_HEAD
		Vector2.UP:
			auxHeadCellUnit = MapReferences.MapUnits.UP_SNAKE_HEAD
		Vector2.DOWN:
			auxHeadCellUnit = MapReferences.MapUnits.DOWN_SNAKE_HEAD
	auxSnakeMat[headCellPos[0] - nCorrection][headCellPos[1] - mCorrection] = auxHeadCellUnit
	
	# find head dir
	for bodyCellPosition in bodyCellPositions:
		auxSnakeMat[bodyCellPosition[0] - nCorrection][bodyCellPosition[1] - mCorrection] = MapReferences.MapUnits.SNAKE_BODY
	
	snakeMatrix = auxSnakeMat
	return snakeMatrix


func _on_Head_entered(area: Area2D) -> void:
	if area.get_parent().is_in_group("Food"):
		eat()
	elif area.get_parent().is_in_group("MapCells"):
		emit_signal("finish_game")
	else:
		receive_damage()
		for b in body.get_children():
			if area == b.get_node("Area2D"):
				emit_signal("finish_game")

func findCurrentHeadCellPos()->Array:
	return MapReferences.position2Grid(head.position)

func collideWithObstacle(removedSnakeBodyCount:int, stunnedSnakeSeconds:float)->void:
	# remove removedSnakeBodyCount snake body pieces
	
	# stun the snake stunnedSnakeSeconds seconds
	stopMovement(stunnedSnakeSeconds)


func _on_Timer_timeout() -> void:
	if(Logger.DEBUG_LOG):
		print("Game should be finished")
	connect("finish_game",Globals,"_on_game_finished")
