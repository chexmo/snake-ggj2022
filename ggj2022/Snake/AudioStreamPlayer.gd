extends AudioStreamPlayer

export var damages = [
	preload("res://Snake/Asset/SFX_DamageOnSnake01.ogg"),  preload("res://Snake/Asset/SFX_DamageOnSnake02.ogg")
]

export var eats = [
	preload("res://Snake/Asset/SFX_SnakeBite01.ogg"),
	preload("res://Snake/Asset/SFX_SnakeBite02.ogg")
]


func play_damage():
	var a = int(rand_range(0,1.9999999))
	stream = damages[a]
	play()

func play_eat():
	var a = int(rand_range(0,1.9999999))
	stream = eats[a]
	play()
