extends Node2D
class_name BodyPiece

var direction : Vector2 = Vector2.RIGHT
var old_position : Vector2 = Vector2.ZERO
var current_position : Vector2 = Vector2.ZERO
var next_position : Vector2 = Vector2.ZERO

onready var animatedSprite:AnimatedSprite = $AnimatedSprite

func cacheReferences() -> void:
	animatedSprite = $AnimatedSprite

func move(target : Vector2, next_target : Vector2) -> void:
	old_position = current_position
	current_position = target
	next_position = next_target
	global_position = current_position


func updateCurrentPos()->void:
	current_position = global_position

func refresh_sprites() -> void:
	
	if (comes_from(Vector2.RIGHT) and goes_to(Vector2.DOWN)) \
			or (comes_from(Vector2.DOWN) and goes_to(Vector2.RIGHT)):
		animatedSprite.animation = "body_c_downright"
	elif (comes_from(Vector2.LEFT) and goes_to(Vector2.DOWN)) \
			or (comes_from(Vector2.DOWN) and goes_to(Vector2.LEFT)):
		animatedSprite.animation = "body_c_downleft"
	elif (comes_from(Vector2.UP) and goes_to(Vector2.RIGHT)) \
			or (comes_from(Vector2.RIGHT) and goes_to(Vector2.UP)):
		animatedSprite.animation = "body_c_upright"
	elif (comes_from(Vector2.UP) and goes_to(Vector2.LEFT)) \
			or (comes_from(Vector2.LEFT) and goes_to(Vector2.UP)):
		animatedSprite.animation = "body_c_upleft"
	else:
		if old_position.y == current_position.y:
			animatedSprite.animation = "body_horizontal"
		else:
			animatedSprite.animation = "body_vertical"
	
	

func comes_from(ref : Vector2) -> bool:
	var a = -1 * (current_position - old_position).normalized()
	return ref == a
	
func goes_to(ref : Vector2) -> bool:
	var a = (next_position - current_position).normalized()
	return ref == a
