extends Control

export var level_scene : PackedScene

onready var creditsRect:ColorRect = $CreditsRect
onready var hoverSFX:AudioStreamPlayer = $HoverSFX
onready var clickSFX:AudioStreamPlayer = $ClickSFX
onready var playSFX:AudioStreamPlayer = $PlaySFX
onready var baseMusic:AudioStreamPlayer = $BaseMusic
onready var music4credits:AudioStreamPlayer = $Music4credits
onready var playBt:TextureButton = $PlayBt


func toggle_music_volumes()->void:
	if creditsRect.visible:
		baseMusic.volume_db=-80
		music4credits.volume_db=-10
	else:
		baseMusic.volume_db=-10
		music4credits.volume_db=-80

func _on_PlayBt_pressed() -> void:
	playBt.disabled = true # disable additional play button clicks
	playSFX.play()
	yield(playSFX,"finished")
	
	var retVal = get_tree().change_scene_to(level_scene)
	if(Logger.DEBUG_LOG):
		print(retVal)


func _on_ExitBt_pressed() -> void:
	clickSFX.play()
	yield(clickSFX,"finished")
	get_tree().quit()


func _on_CreditsBt_pressed() -> void:
	clickSFX.play()
	creditsRect.visible = not creditsRect.visible
	toggle_music_volumes()
	

func _on_any_button_hovered() -> void:
	hoverSFX.play()
