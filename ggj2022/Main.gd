extends Node2D

export var scene : PackedScene

func _ready() -> void:
	if !scene : 
		return
	var retVal = print(get_tree().change_scene_to(scene))
	if(Logger.DEBUG_LOG):
		print(retVal)
