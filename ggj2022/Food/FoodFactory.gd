extends Node

const Fruit:PackedScene = preload("res://Food/Fruit.tscn")
const Earthworm:PackedScene = preload("res://Food/Earthworm.tscn")
const Rat:PackedScene = preload("res://Food/Rat.tscn")

const happyFood:Array = [Fruit, Earthworm, Rat]
const evilFood:Array = [Fruit, Earthworm, Rat]

static func instantiateRandomFood(parent, inHappyMap:bool)->Node2D:
	assert(parent != null)
	assert(happyFood != null)
	assert(happyFood != [])
	assert(evilFood != null)
	assert(evilFood != [])
	
	var obstacle:Node2D
	if(inHappyMap):
		obstacle = happyFood[randi() % happyFood.size()].instance()
	else:
		obstacle = evilFood[randi() % evilFood.size()].instance()
	
	parent.add_child(obstacle)
	return obstacle
