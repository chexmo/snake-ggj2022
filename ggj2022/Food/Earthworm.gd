extends Node2D

export var speed : float = 7
export var ray_distance : int = 70
export var points: int = 100
export var label_time : float = 3.0

onready var animatedSprite:AnimatedSprite = $AnimatedSprite
onready var rayCast2D:RayCast2D = $RayCast2D
onready var label:Label = $Label
onready var timer:Timer = $Timer
onready var collisionShape2D:CollisionShape2D = $Area2D/CollisionShape2D

var velocity : Vector2 = Vector2.ZERO

func _ready() -> void:
	timer.one_shot = true # it only has to fire one time with no repetition
	velocity = Vector2.RIGHT * ( (randi()%2) * 2 - 1) * speed
	if(Logger.DEBUG_LOG):
		printt("Gusano - Velocity:", velocity)

func _process(delta: float) -> void:
	if rayCast2D.is_colliding():
		if(Logger.DEBUG_LOG):
			printt("Gusano - Collided", rayCast2D.get_collision_point())
		velocity *= -1
	position += velocity * delta
	rayCast2D.cast_to = ray_distance * velocity.normalized()
	
	if velocity.x > 0:
		animatedSprite.animation = "move_right"
	else:
		animatedSprite.animation = "move_left"


func _on_Area2D_area_entered(area: Area2D) -> void:
	if area.get_parent().is_in_group("serpiente"):
		animatedSprite.visible = false
		Globals.points += points
		label.text = str(points)
		label.visible = true
		collisionShape2D.disabled = true # prevents unwanted collisions
		timer.start(label_time) # it will free resources after timer timeout

func _on_Timer_timeout() -> void:
	queue_free()
