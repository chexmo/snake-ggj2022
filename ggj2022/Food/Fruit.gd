extends Node2D

export var multiplier : int = 20
export var label_time : float = 3.0
export var textures = [
	preload("res://Food/Textures/AppleTex.tres"),
	preload("res://Food/Textures/BananaTex.tres"),
	preload("res://Food/Textures/OrangeTex.tres")
]

onready var sprite:Sprite = $Sprite
onready var label:Label = $Label
onready var timer:Timer = $Timer
onready var collisionShape2D:CollisionShape2D = $Area2D/CollisionShape2D

var selected : int = 0
var points : int = 0


func _ready() -> void:
	timer.one_shot = true # it only has to fire one time with no repetition
	selected = randi()%textures.size()
	sprite.texture = textures[selected]
	points = (selected + 2) * multiplier


func _on_Area2D_area_entered(area: Area2D) -> void:
	if area.get_parent().is_in_group("serpiente"):
		sprite.visible = false
		Globals.points += points
		label.text = str(points)
		label.visible = true
		collisionShape2D.disabled = true # prevents unwanted collisions
		timer.start(label_time) # it will free resources after timer timeout


func _on_Timer_timeout() -> void:
	queue_free()
