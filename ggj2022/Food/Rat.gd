extends Node2D

export var speed : float = 100
export var ray_distance : int = 70
export var think_time : float = 0.5
export var points: int = 100
export var label_time : float = 3.0

onready var deathTimer:Timer = $DeathTimer
onready var animatedSprite:AnimatedSprite = $AnimatedSprite
onready var rayCast2D:RayCast2D = $RayCast2D
onready var label:Label = $Label
onready var timer:Timer = $Timer
onready var collisionShape2D:CollisionShape2D = $Area2D/CollisionShape2D

var velocity : Vector2 = Vector2.ZERO
var aims_to : Vector2 = Vector2.ZERO
var is_colliding : bool = false


func _ready() -> void:
	change_direction()
	animate()
	velocity = aims_to * speed

func _process(delta: float) -> void:
	if rayCast2D.is_colliding():
		if(Logger.DEBUG_LOG):
			printt("Rata - Collided", rayCast2D.get_collision_point())
		if not is_colliding:
			is_colliding = true
			velocity = Vector2.ZERO
			timer.start(think_time)
			change_direction()
			is_colliding = false
	else:
		is_colliding = false
		
	position += velocity * delta

func animate():
	match aims_to:
		Vector2.RIGHT: animatedSprite.play("move_right")
		Vector2.LEFT:  animatedSprite.play("move_left")
		Vector2.DOWN:  animatedSprite.play("move_down")
		Vector2.UP:    animatedSprite.play("move_up")

func change_direction() -> void :
	var old_direction = aims_to
	while aims_to == old_direction:
		match randi() % 4:
			0: aims_to = Vector2.RIGHT
			1: aims_to = Vector2.DOWN
			2: aims_to = Vector2.LEFT
			3: aims_to = Vector2.UP
	if(Logger.DEBUG_LOG):
		printt("Aiming to", aims_to, aims_to.angle())
	rayCast2D.cast_to = aims_to * ray_distance


func _on_Timer_timeout() -> void:
	velocity = aims_to * speed
	animate()

func _on_Area2D_area_entered(area: Area2D) -> void:
	if area.get_parent().is_in_group("serpiente"):
		animatedSprite.visible = false
		Globals.points += points
		label.text = str(points)
		label.visible = true
		collisionShape2D.disabled = true # prevents unwanted collisions
		timer.start(label_time) # it will free resources after timer timeout


func _on_DeathTimer_timeout() -> void:
	queue_free()
