extends Node

const Bottle:PackedScene = preload("res://Obstacle/Concrete/Bottle.tscn")
const Mine:PackedScene = preload("res://Obstacle/Concrete/Mine.tscn")
const Rock:PackedScene = preload("res://Obstacle/Concrete/Rock.tscn")
const TrashCan:PackedScene = preload("res://Obstacle/Concrete/TrashCan.tscn")
const HappyTree1:PackedScene = preload("res://Obstacle/Concrete/HappyTree1.tscn")
const HappyTree2:PackedScene = preload("res://Obstacle/Concrete/HappyTree2.tscn")
const EvilTree:PackedScene = preload("res://Obstacle/Concrete/EvilTree.tscn")

const happyObstacles:Array = [Rock, TrashCan, HappyTree1, HappyTree2]
const evilObstacles:Array = [Mine, Bottle, EvilTree]

static func instantiateRandomObstacle(parent, inHappyMap:bool)->Node2D:
	assert(parent != null)
	assert(happyObstacles != null)
	assert(happyObstacles != [])
	assert(evilObstacles != null)
	assert(evilObstacles != [])
	
	var obstacle:Node2D
	if(inHappyMap):
		obstacle = happyObstacles[randi() % happyObstacles.size()].instance()
	else:
		obstacle = evilObstacles[randi() % evilObstacles.size()].instance()
	
	parent.add_child(obstacle)
	return obstacle

