extends Node2D

const Snake = preload("res://Snake/Snake.gd")

export(int,10) var _removedSnakeBodyCount
export(float,5) var _stunnedSnakeSeconds

onready var sprite:Sprite = $Sprite
onready var collision:CollisionShape2D = $Area2D/Collision


func _on_Area2D_area_entered(area: Area2D) -> void: # this area will touch another one
	var collidedNode:Node2D = area.get_parent()
	if area.get_parent().is_in_group("serpiente"):
		var collidedSnake:Snake = collidedNode.get_parent() as Snake
		collidedSnake.collideWithObstacle(_removedSnakeBodyCount, _stunnedSnakeSeconds)
		sprite.visible = false
		collision.disabled = true
		queue_free() # free resources


