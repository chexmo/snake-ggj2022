extends Node

const MapMatrixPrinter = preload("res://Map/MapMatrix/MapMatrixPrinter.gd")
const BaseMapMatrixGenerator = preload("res://Map/MapMatrix/BaseMapMatrixGenerator.gd")
const ObstacleFactory = preload("res://Obstacle/ObstacleFactory.gd")
const FoodFactory = preload("res://Food/FoodFactory.gd")

const MapCell = preload("res://Map/MapCell/MapCell.tscn")

const BASE_EVIL_NO_TRANS_DEL_PERC = 0.4
const EXTRA_EVIL_NO_TRANS_DEL_PERC = 0.3

const BASE_HAPPY_NO_TRANS_DEL_PERC = 0.1
const EXTRA_HAPPY_NO_TRANS_DEL_PERC = 0.1

# ---------<OBSTACLES>-----------

const BASE_EVIL_OBSTACLE_CELL_PERCENTAGE = 0.1
const EXTRA_EVIL_OBSTACLE_CELL_PERCENTAGE = 0.1

const BASE_HAPPY_OBSTACLE_CELL_PERCENTAGE = 0.025
const EXTRA_HAPPY_OBSTACLE_CELL_PERCENTAGE = 0.025

# ---------</OBSTACLES>-----------

# ---------<FOOD>-----------

const BASE_EVIL_FOOD_CELL_PERCENTAGE = 0.025
const EXTRA_EVIL_FOOD_CELL_PERCENTAGE = 0.025

const BASE_HAPPY_FOOD_CELL_PERCENTAGE = 0.1
const EXTRA_HAPPY_FOOD_CELL_PERCENTAGE = 0.1

# ---------</FOOD>-----------

const BASE_MATRIX_GROUP_SIZE = 2
const EXTRA_MATRIX_GROUP_SIZE = 1

const BASE_MATRIX_SHAPE_SIZE = 4
const EXTRA_MATRIX_SHAPE_SIZE = 2


var baseMapDataMatrix:Array = [] # without snake
var mapWithSnakeDataMatrix:Array = [] # with snake
var snake:Node2D
var mapCells:Node2D
var obstacles:Node2D
var food:Node2D

var inHappyMap:bool = false

onready var timer:Timer = $Timer


func startMapGeneration(snake:Node2D, mapCells:Node2D, obstacles:Node2D, food:Node2D)->void:
	self.snake = snake
	self.mapCells = mapCells
	self.obstacles = obstacles
	self.food = food
	generateNewMap()
	timer.one_shot = false
	timer.connect("timeout", self, "generateNewMap")
	timer.set_wait_time(8)
	timer.start()




func generateNewMap()->void:
	var oldSnakeHeadPosAndDir:Array = []
	var oldSnakeHeadPos:Array = []
	if(mapWithSnakeDataMatrix != []):
		oldSnakeHeadPosAndDir = snake.findCurrentHeadCellPos() # current point in the map
		if(oldSnakeHeadPosAndDir != []):
			oldSnakeHeadPos = [oldSnakeHeadPosAndDir[0], oldSnakeHeadPosAndDir[1]]
	
	if(!inHappyMap):
		inHappyMap = true  # evil -> happy
		generateBrandNewMapMatrix()
	else:
		inHappyMap = false # happy -> evil
		generateMapMatrixFromBaseMap() # it will only alternate difficulty on base map
	
	
	updateSnakePositions(oldSnakeHeadPos) # properly locates snake in the map
	
	mapWithSnakeDataMatrix = BaseMapMatrixGenerator.obtainMapMatrixWithSnakeMapEntrance(mapWithSnakeDataMatrix)
	
	var currTransCellCount = BaseMapMatrixGenerator.obtainCurrentTransitableCellCount(mapWithSnakeDataMatrix)
	# add food
	[mapWithSnakeDataMatrix, currTransCellCount] = BaseMapMatrixGenerator.obtainMapMatrixWithFood(mapWithSnakeDataMatrix, currTransCellCount, obtainCurrentFoodCellPercentage())
	# add obstacles
	[mapWithSnakeDataMatrix, currTransCellCount] = BaseMapMatrixGenerator.obtainMapMatrixWithObstacles(mapWithSnakeDataMatrix, currTransCellCount, obtainCurrentObstacleCellPercentage())
	# add enemies
	instanceMapCells(mapWithSnakeDataMatrix)
	
	
	if(Logger.DEBUG_LOG):
		print(mapWithSnakeDataMatrix)
		MapMatrixPrinter.savePrettyMatrixInFile(mapWithSnakeDataMatrix)
		print(MapMatrixPrinter.matrix2String(mapWithSnakeDataMatrix))
	
	
	if(inHappyMap): 
		EventManager.emit_happy_started()
	else: 
		EventManager.emit_evil_started()
	


func updateSnakePositions(oldSnakeHeadPos:Array)->void:
	var currentSnakeHeadPos:Array = []
	if(oldSnakeHeadPos != []):
		var currentSnakeHeadPosAndDir:Array = findSnakeHeadPosAndDir() # spawn point snake head pos and dir
		if(currentSnakeHeadPosAndDir != []):
			currentSnakeHeadPos = [currentSnakeHeadPosAndDir[0], currentSnakeHeadPosAndDir[1]]
	
	var snakePosOffset:Array = []
	if(oldSnakeHeadPos !=  [] and currentSnakeHeadPos != []):
		# this is not the initial snake spawn -> check difference with prev snake head position
		snakePosOffset = [currentSnakeHeadPos[0] - oldSnakeHeadPos[0], currentSnakeHeadPos[1] - oldSnakeHeadPos[1]]
	else:
		snakePosOffset = findSnakeMatrixUpperleftmostPoint() # this is the initial snake spawn
	snake.updatePositions(snakePosOffset)



func generateBrandNewMapMatrix()->void:
	baseMapDataMatrix = BaseMapMatrixGenerator.getRandomlyShapedBaseMapMatrix( \
	BASE_MATRIX_SHAPE_SIZE + (0 if (EXTRA_MATRIX_SHAPE_SIZE == 0) else randi() % EXTRA_MATRIX_SHAPE_SIZE), \
	BASE_MATRIX_SHAPE_SIZE + (0 if (EXTRA_MATRIX_SHAPE_SIZE == 0) else randi() % EXTRA_MATRIX_SHAPE_SIZE), \
	BASE_MATRIX_GROUP_SIZE + (0 if (EXTRA_MATRIX_GROUP_SIZE == 0) else randi() % EXTRA_MATRIX_GROUP_SIZE), \
	BASE_MATRIX_GROUP_SIZE + (0 if (EXTRA_MATRIX_GROUP_SIZE == 0) else randi() % EXTRA_MATRIX_GROUP_SIZE))
	
	generateMapMatrixFromBaseMap()

func generateMapMatrixFromBaseMap()->void:
	get_tree().call_group("MapCells", "queue_free") # dealocs memory from cells
	get_tree().call_group("Obstacles", "queue_free") # dealocs memory from obstacles 
	get_tree().call_group("Food", "queue_free") # dealocs memory from food
	
#	baseMapDataMatrix = BaseMapMatrixGenerator.getBaseMapMatrix([[0,1,1],
#															[0,1,0],
#															[0,1,1]],BASE_MATRIX_GROUP_SIZE + randi() % EXTRA_MATRIX_GROUP_SIZE, 
#																			BASE_MATRIX_GROUP_SIZE + randi() % EXTRA_MATRIX_GROUP_SIZE)
	
	var auxMapMat:Array = BaseMapMatrixGenerator.getMapMatrixWithSnake(baseMapDataMatrix, snake.snake2Matrix())
	auxMapMat = BaseMapMatrixGenerator.deleteSomeNoTransitableCells(auxMapMat, obtainCurrentNoTransDeletePercentage() )
	
	mapWithSnakeDataMatrix = auxMapMat # updates current map


func instanceMapCells(mapMatrix:Array)->void:
	var n:int = mapMatrix.size()
	var m:int = mapMatrix[0].size()
	var auxNode:Node2D
	var auxPosition:Vector2
	var transitableRight:bool
	var transitableLeft:bool
	var transitableUp:bool
	var transitableDown:bool
	var cellIsTransitable:bool
	var cellHasObstacle:bool
	var cellHasFood:bool
	
	for i in range(n):
		for j in range(m):
			auxNode = MapCell.instance()
			auxPosition = Vector2(0 + MapReferences.CELL_X_SIZE * j, 0 + MapReferences.CELL_Y_SIZE * i)
			auxNode.position = auxPosition
			mapCells.add_child(auxNode)
			transitableDown = false
			transitableLeft = false
			transitableRight = false
			transitableUp = false
			cellIsTransitable = false
			cellHasObstacle = mapMatrix[i][j] == MapReferences.MapUnits.OBSTACLE
			cellHasFood = mapMatrix[i][j] == MapReferences.MapUnits.FOOD
			if(cellHasObstacle || cellHasFood || MapReferences.isTransitableOrSnakeCell(mapMatrix[i][j])):
				cellIsTransitable = true
				transitableDown = true
				transitableLeft = true
				transitableRight = true
				transitableUp = true
				# uncomment below lines if border art is improved for transitable cells that have no transitable neighbours
#				transitableUp = (i > 0) and MapReferences.isTransitableOrSnakeCell(mapMatrix[i - 1][j]) 
#				transitableDown = (i < n - 1) and MapReferences.isTransitableOrSnakeCell(mapMatrix[i + 1][j])
#				transitableLeft = (j > 0) and MapReferences.isTransitableOrSnakeCell(mapMatrix[i][j - 1])
#				transitableRight = (j < m - 1) and MapReferences.isTransitableOrSnakeCell(mapMatrix[i][j + 1])
			auxNode.setInHappyMap(inHappyMap)
			auxNode.setup(cellIsTransitable, transitableRight, transitableLeft, transitableUp, transitableDown)
			
			if(cellHasObstacle):
				auxNode = ObstacleFactory.instantiateRandomObstacle(obstacles, inHappyMap)
				auxNode.position = auxPosition
			if(cellHasFood):
				auxNode = FoodFactory.instantiateRandomFood(food, inHappyMap)
				auxNode.position = auxPosition




func findSnakeHeadPosAndDir()->Array:
	return BaseMapMatrixGenerator.findSnakeHeadPosAndDir(mapWithSnakeDataMatrix)

# it will find left most and upmost snake spawn zone indexes
func findSnakeMatrixUpperleftmostPoint()->Array:
	return BaseMapMatrixGenerator.findSnakeMatrixUpperleftmostPoint(mapWithSnakeDataMatrix)


func obtainCurrentDifficultyParameter(happyParameter:float, evilParameter:float)->float:
	if(inHappyMap):
		return happyParameter
	else:
		return evilParameter

func obtainCurrentNoTransDeletePercentage()->float:
	return obtainCurrentDifficultyParameter(1 - BASE_HAPPY_NO_TRANS_DEL_PERC + randf() * EXTRA_HAPPY_NO_TRANS_DEL_PERC, \
											1 - BASE_EVIL_NO_TRANS_DEL_PERC + randf() * EXTRA_EVIL_NO_TRANS_DEL_PERC)

func obtainCurrentObstacleCellPercentage()->float:
	return obtainCurrentDifficultyParameter(BASE_HAPPY_OBSTACLE_CELL_PERCENTAGE + randf() * EXTRA_HAPPY_OBSTACLE_CELL_PERCENTAGE, \
											BASE_EVIL_OBSTACLE_CELL_PERCENTAGE + randf() * EXTRA_EVIL_OBSTACLE_CELL_PERCENTAGE)

func obtainCurrentFoodCellPercentage()->float:
	return obtainCurrentDifficultyParameter(BASE_HAPPY_FOOD_CELL_PERCENTAGE + randf() * EXTRA_HAPPY_FOOD_CELL_PERCENTAGE, \
											BASE_EVIL_FOOD_CELL_PERCENTAGE + randf() * EXTRA_EVIL_FOOD_CELL_PERCENTAGE)





