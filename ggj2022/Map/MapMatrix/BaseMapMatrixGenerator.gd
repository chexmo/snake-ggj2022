extends Node

const MapMatrixGenerator = preload("res://Map/MapMatrix/MapMatrixBlockGroupGenerator.gd")
const MapMatrixPrinter = preload("res://Map/MapMatrix/MapMatrixPrinter.gd")


const MIN_EXTRA_ROW_COUNT = 2 # 2 minimun to get enough space to maneuver
const MIN_EXTRA_COLUMN_COUNT = 2 # 2 minimun to get enough space to maneuver
const TARGET_MAP_ENTRANCE_CELLS = 3 # minimun transitable or no transitable cells replaced by map entrance ones


static func getMapMatrixWithSnake(origMapMat:Array, snakeMatrix:Array):
	assert(origMapMat != null)
	assert(origMapMat != [])
	assert(snakeMatrix != null)
	assert(snakeMatrix != [])
	
	
	var snakeHeadPosAndDir:Array = findSnakeHeadPosAndDir(snakeMatrix)
	
	if(snakeHeadPosAndDir == []):
		return []
	
	var snakeHeadPos:Array = [snakeHeadPosAndDir[0], snakeHeadPosAndDir[1]]
	var snakeHeadDir:int= snakeHeadPosAndDir[2]
	
	snakeMatrix[snakeHeadPos[0]][snakeHeadPos[1]] = snakeHeadDir # updates with latest head dir
	
	var mapEntryPoint:Array = findMapEntryPoint(origMapMat, snakeMatrix, snakeHeadPos, snakeHeadDir)
	
	if(mapEntryPoint == []):
		return []
	
	var newMapMat:Array = []
	
	newMapMat = AppendSnakeToMap(origMapMat, snakeMatrix, snakeHeadPos, mapEntryPoint, snakeHeadDir)
	
	return newMapMat


static func getRandomlyShapedBaseMapMatrix(shapeN:int, shapeM:int, n:int, m: int)->Array:
	assert(shapeN > 0)
	assert(shapeM > 0)
	assert(n > 0)
	assert(m > 0)
	
	return MapMatrixGenerator.getBlockGroupsWithShape(getRandomShapeMatrix(shapeN, shapeM), n, m)


static func getRandomShapeMatrix(n: int, m: int)->Array:
	assert(n > 0)
	assert(m > 0)
	
	var auxMat:Array = MapMatrixGenerator.matrixOfSameValues(n, m, 0)
	
	var i:int = randi() % n
	auxMat[i][0] = 1
	var nextI:int = -1
	for j in range(m):
		auxMat[i][j] = 1
		nextI = -1
		if(i>0 and randi() % 2 == 0):
			nextI = i - 1
			auxMat[nextI][j] = 1
		if(i < n - 1 and randi() % 2 == 0):
			if(nextI == -1):
				nextI = i + 1
			auxMat[i + 1][j] = 1
		if(nextI == -1 ):
			nextI = i
		
		i = nextI
	
	
	return auxMat


# it finds snake head position and fixed direction (corrected if needed to avoid collision with snake body)
static func findSnakeHeadPosAndDir(mapMatrix:Array)->Array:
	assert(mapMatrix != null)
	assert(mapMatrix != [])
	
	var result:Array = []
	var n:int = mapMatrix.size()
	var m:int = mapMatrix[0].size()
	
	var i: int = 0
	var j: int
	var headPosition:Array = []
	var headDir:int = -1
	while (i < n and headPosition == []):
		j = 0
		while (j < m and headPosition == []):
			if(MapReferences.isSnakeHeadCell(mapMatrix[i][j])):
					headPosition = [i, j]
					headDir = mapMatrix[i][j]
			j += 1
		i += 1
	
	if(headPosition == []):
		return []
	
	
	if(!snakeCollidesWithBody(mapMatrix, headPosition, headDir)):
		return headPosition + [headDir]
	else:
		var nonCollidingDirs:Array = ObtainNonCollidingDirs(mapMatrix, headPosition, headDir)
		
		if(nonCollidingDirs.size() > 0):
			return headPosition + [nonCollidingDirs[randi() % nonCollidingDirs.size()]]
		else: # it will collide no mater which direction is used -> should we change snake shape?
			return headPosition + [headDir]



static func findSnakeMatrixUpperleftmostPoint(mapWithSnakeDataMatrix:Array)->Array:
	assert(mapWithSnakeDataMatrix != null)
	assert(mapWithSnakeDataMatrix != [])
	
	var result:Array = []
	var n:int = mapWithSnakeDataMatrix.size()
	var m:int = mapWithSnakeDataMatrix[0].size()
	
	var i: int = 0
	var j: int
	var upperleftmostPoint:Array = []
	while (i < n and upperleftmostPoint == []):
		j = 0
		while (j < m and upperleftmostPoint == []):
			if(MapReferences.isSpawnRelatedCell(mapWithSnakeDataMatrix[i][j]) and ! mapWithSnakeDataMatrix[i][j] == MapReferences.MapUnits.MAP_ENTRANCE):
					upperleftmostPoint = [i, j]
			j += 1
		i += 1
	
	return upperleftmostPoint




static func snakeCollidesWithBody(snakeMatrix:Array, headPosition:Array, headDir:int)->bool:
	assert(snakeMatrix != null)
	assert(snakeMatrix != [])
	assert(headPosition != null)
	assert(headPosition != [])
	assert(MapReferences.isSnakeHeadCell(headDir))
	
	var collidesWithBody:bool = false
	var n:int = snakeMatrix.size()
	var m:int = snakeMatrix[0].size()
	
	
	match(headDir):
		MapReferences.MapUnits.RIGHT_SNAKE_HEAD:
			var auxRow:int = headPosition[0]
			for j1 in range(headPosition[1] + 1, m):
				if(snakeMatrix[auxRow][j1] == MapReferences.MapUnits.SNAKE_BODY):
					collidesWithBody = true
					break
		MapReferences.MapUnits.LEFT_SNAKE_HEAD:
			var auxRow:int = headPosition[0]
			for j1 in range(headPosition[1] - 1, -1, -1):
				if(snakeMatrix[auxRow][j1] == MapReferences.MapUnits.SNAKE_BODY):
					collidesWithBody = true
					break
		MapReferences.MapUnits.UP_SNAKE_HEAD:
			var auxColumn:int = headPosition[1]
			for i1 in range(headPosition[0] - 1, -1, -1):
				if(snakeMatrix[i1][auxColumn] == MapReferences.MapUnits.SNAKE_BODY):
					collidesWithBody = true
					break
		MapReferences.MapUnits.DOWN_SNAKE_HEAD:
			var auxColumn:int = headPosition[1]
			for i1 in range(headPosition[0] + 1, n):
				if(snakeMatrix[i1][auxColumn] == MapReferences.MapUnits.SNAKE_BODY):
					collidesWithBody = true
					break
	
	return collidesWithBody

static func ObtainNonCollidingDirs(snakeMatrix:Array, headPosition:Array, wrongHeadDir:int)->Array:
	assert(snakeMatrix != null)
	assert(snakeMatrix != [])
	assert(headPosition != null)
	assert(headPosition != [])
	assert(MapReferences.isSnakeHeadCell(wrongHeadDir))
	
	var auxDirs:Array = []
	
	auxDirs = [MapReferences.MapUnits.RIGHT_SNAKE_HEAD, MapReferences.MapUnits.LEFT_SNAKE_HEAD, \
			  MapReferences.MapUnits.UP_SNAKE_HEAD, MapReferences.MapUnits.DOWN_SNAKE_HEAD]
	
	auxDirs.erase(wrongHeadDir)
	var i:int = auxDirs.size() - 1
	while(i >= 0):
		if(snakeCollidesWithBody(snakeMatrix, headPosition, auxDirs[i])):
			auxDirs.remove(i)
		i -= 1
	
	return auxDirs


# finds a snakeEntry point that complies with map matrix and snake constrains
static func findMapEntryPoint(origMapMat:Array, snakeMatrix:Array, snakeHeadPos:Array, snakeHeadDir:int)->Array:
	assert(origMapMat != null)
	assert(origMapMat != [])
	assert(snakeMatrix != null)
	assert(snakeMatrix != [])
	assert(snakeHeadPos != null)
	assert(snakeHeadPos != [])
	assert(MapReferences.isSnakeHeadCell(snakeHeadDir))
	
	var auxEntryPoints:Array = []
	var origMapMatN:int = origMapMat.size()
	var origMapMatM:int = origMapMat[0].size()
	match(snakeHeadDir):
		MapReferences.MapUnits.RIGHT_SNAKE_HEAD:
			auxEntryPoints = obtainColumnBasedEntryPoints(origMapMat, 0, 1)
		MapReferences.MapUnits.LEFT_SNAKE_HEAD:
			auxEntryPoints = obtainColumnBasedEntryPoints(origMapMat, origMapMatM - 1, -1)
		MapReferences.MapUnits.UP_SNAKE_HEAD:
			auxEntryPoints = obtainRowBasedEntryPoints(origMapMat, origMapMatN - 1, -1)
		MapReferences.MapUnits.DOWN_SNAKE_HEAD:
			auxEntryPoints = obtainRowBasedEntryPoints(origMapMat, 0, 1)
	
	
	if(auxEntryPoints != []):
		return auxEntryPoints[randi() % auxEntryPoints.size()] 
	else:
		return [0,0]


# entry points that are obtained via consecutive transitable cells on the same column (rightmost or leftmost map entrance)
static func obtainColumnBasedEntryPoints(mapMat:Array, initialColumn:int, increment:int)->Array:
	assert(mapMat != null)
	assert(mapMat != [])
	
	var entryPoints:Array = []
	var i:int = 0
	var j:int = initialColumn
	var n:int = mapMat.size()
	var m:int = mapMat[0].size()
	var adjacentTransCellCount: int
	var maxAdjTransCellCount:int = 0
	var adjacentNoTrans:int = 0
	var lastAdjTransCellIndex:int = MapReferences.INVALID_INDEX
	
	while(j < m and j >= 0):
		i = 0
		
		adjacentNoTrans = 0
		while(i < n and adjacentNoTrans < 2):
			adjacentNoTrans = 0
			
			while(i < n and !MapReferences.isNoTransitableCell(mapMat[i][j])):
				i += 1
			
			while(i < n and MapReferences.isNoTransitableCell(mapMat[i][j]) and adjacentNoTrans < 2):
				i += 1
				adjacentNoTrans += 1
			
		
		if(adjacentNoTrans != 2):
			j += increment
			continue
		
		if(i > 0): 
			i -= 1 # it has to go back because it moved down unnecessarily
		j += increment # it should be transitable road on this column
		
		if(j >= m or j < 0):
			continue # it has to exit the current iteration
		
		while(i < n): # it will find all possible entry points located on j column
			while(i < n and mapMat[i][j] != MapReferences.MapUnits.TRANSITABLE):
				i += 1
			adjacentTransCellCount = 0
			while(i < n and mapMat[i][j] == MapReferences.MapUnits.TRANSITABLE and MapReferences.isNoTransitableCell(mapMat[i][j - increment]) ):
				adjacentTransCellCount += 1 
				i += 1
			lastAdjTransCellIndex = i
			while(i < n and mapMat[i][j] == MapReferences.MapUnits.TRANSITABLE):
				i += 1
			
			if(adjacentTransCellCount > maxAdjTransCellCount):
				maxAdjTransCellCount = adjacentTransCellCount
				entryPoints = [[obtainEntryIndex(lastAdjTransCellIndex, adjacentTransCellCount), j - increment]]
			elif(adjacentTransCellCount > 0 and adjacentTransCellCount == maxAdjTransCellCount):
				entryPoints.append([obtainEntryIndex(lastAdjTransCellIndex, adjacentTransCellCount), j - increment])
		
		j += increment # all entryPoints on this column were added
	
	return entryPoints


# entry points that are obtained via consecutive transitable cells on the same row (upmost or downmost map entrance)
static func obtainRowBasedEntryPoints(mapMat:Array, initialRow:int, increment:int)->Array:
	assert(mapMat != null)
	assert(mapMat != [])
	
	var entryPoints:Array = []
	var i:int = initialRow
	var j:int = 0
	var n:int = mapMat.size()
	var m:int = mapMat[0].size()
	var adjacentTransCellCount: int
	var maxAdjTransCellCount:int = 0
	var adjacentNoTrans:int = 0
	var lastAdjTransCellIndex:int = MapReferences.INVALID_INDEX
	
	while(i < n and i >= 0):
		j = 0
		
		adjacentNoTrans = 0
		while(j < m and adjacentNoTrans < 2):
			adjacentNoTrans = 0
			
			while(j < m and !MapReferences.isNoTransitableCell(mapMat[i][j])):
				j += 1
			
			while(j < m and MapReferences.isNoTransitableCell(mapMat[i][j]) and adjacentNoTrans < 2):
				j += 1
				adjacentNoTrans += 1
			
		
		if(adjacentNoTrans != 2):
			i += increment
			continue
		
		if(j > 0): 
			j -= 1 # it has to go back because it moved down unnecessarily
		i += increment # it should be transitable road on this column
		
		if(i >= n or i < 0):
			continue # it has to exit the current iteration
		
		while(j < m): # it will find all possible entry points located on j column
			while(j < m and mapMat[i][j] != MapReferences.MapUnits.TRANSITABLE):
				j += 1
			adjacentTransCellCount = 0
			while(j < m and mapMat[i][j] == MapReferences.MapUnits.TRANSITABLE and MapReferences.isNoTransitableCell(mapMat[i - increment][j]) ):
				adjacentTransCellCount += 1 
				j += 1
			lastAdjTransCellIndex = j
			while(j < m and mapMat[i][j] == MapReferences.MapUnits.TRANSITABLE):
				j += 1
			
			if(adjacentTransCellCount > maxAdjTransCellCount):
				maxAdjTransCellCount = adjacentTransCellCount
				entryPoints = [[i - increment, obtainEntryIndex(lastAdjTransCellIndex, adjacentTransCellCount)]]
			elif(adjacentTransCellCount > 0 and adjacentTransCellCount == maxAdjTransCellCount):
				entryPoints.append([i - increment, obtainEntryIndex(lastAdjTransCellIndex, adjacentTransCellCount)])
		
		i += increment # all entryPoints on this column were added
	
	return entryPoints



static func obtainEntryIndex(currentIndex:int, curAdjacentTransCellCount:int)->int:
	var extraNoise:int = 1 if ((randi() % 2) == 0) else -1 # a displacement from ideal index
	
	return currentIndex - 1 - curAdjacentTransCellCount / 2 + extraNoise


static func AppendSnakeToMap(origMapMat:Array, snakeMatrix:Array, snakeHeadPos:Array, mapEntryPoint:Array, snakeHeadDir:int):
	assert(origMapMat != null)
	assert(origMapMat != [])
	assert(snakeMatrix != null)
	assert(snakeMatrix != [])
	
	var expandedMap:Array = []
	var mapN:int = origMapMat.size()
	var mapM:int = origMapMat[0].size()
	
	
	
	
	snakeHeadPos = [snakeHeadPos[0] + 1, snakeHeadPos[1] + 1] # fixed for extended snake matrix
	snakeMatrix = ObtainSorroundedSnake(snakeMatrix, snakeHeadPos)
	
	var snakeN:int = snakeMatrix.size()
	var snakeM:int = snakeMatrix[0].size()
	
	var extraUpRowCount: int = snakeHeadPos[0] - mapEntryPoint[0]
	var extraDownRowCount: int = snakeN - (mapN + extraUpRowCount)
	
	var extraLeftColumnCount: int = snakeHeadPos[1] - mapEntryPoint[1]
	var extraRightColumnCount: int = snakeM - (mapM + extraLeftColumnCount)
	
	var newN:int = mapN + max(extraUpRowCount, 0) + max(extraDownRowCount, 0) + 2 # +2 due to snake borders
	var newM:int = mapM + max(extraLeftColumnCount, 0) + max(extraRightColumnCount, 0) + 2 # +2 due to snake borders
	
	# -> calcular posicion en el mapa grande de la esquina sup izq de la serpiente
	
	expandedMap = MapMatrixGenerator.matrixOfSameValues(newN, newM, MapReferences.MapUnits.EMPTY)
	
	
	
	var rowOffset = -extraUpRowCount + 1 if (extraUpRowCount < 0) else 0 
	var columnOffset = -extraLeftColumnCount + 1 if (extraLeftColumnCount < 0) else 0
	var fixedSnakeHeadPos:Array = [snakeHeadPos[0] + rowOffset, snakeHeadPos[1] + columnOffset] 
	var newSnakeHeadPos:Array = []
	
	# magic offsets
	var columnExtraOffset:int = 0
	if(snakeHeadDir == MapReferences.MapUnits.RIGHT_SNAKE_HEAD):
		columnExtraOffset = 1
	elif(snakeHeadDir == MapReferences.MapUnits.LEFT_SNAKE_HEAD):
		columnExtraOffset = -1
	
	var rowExtraOffset:int = 0
	if(snakeHeadDir == MapReferences.MapUnits.UP_SNAKE_HEAD):
		rowExtraOffset = -1
	elif(snakeHeadDir == MapReferences.MapUnits.DOWN_SNAKE_HEAD):
		rowExtraOffset = 1
	
	# original map rebuilding parameters
	var iniMapRow:int
	var iniMapColumn:int
	var maxMapRow:int
	var maxMapColumn:int
	
	
	for i in range(rowOffset, snakeN + rowOffset):
		for j in range(columnOffset, snakeM + columnOffset):
			expandedMap[i][j] = snakeMatrix[i - rowOffset][j - columnOffset]
			
			if(MapReferences.isSnakeHeadCell(expandedMap[i][j])):
				newSnakeHeadPos = [i, j]
				iniMapRow = i - mapEntryPoint[0] + rowExtraOffset
				iniMapColumn = j - mapEntryPoint[1] + columnExtraOffset
				
				maxMapRow = iniMapRow + mapN
				maxMapColumn = iniMapColumn + mapM
				
	
	# add original map to expanded one
	for l in range(iniMapRow, maxMapRow):
		for k in range(iniMapColumn, maxMapColumn):
			if(!MapReferences.isSpawnRelatedCell(expandedMap[l][k]) or \
			   (MapReferences.isNoTransitableCell(expandedMap[l][k]) and  origMapMat[l - iniMapRow][k - iniMapColumn] == MapReferences.MapUnits.TRANSITABLE) \
			   or (expandedMap[l][k] == MapReferences.MapUnits.HARD_NO_TRANSITABLE and origMapMat[l - iniMapRow][k - iniMapColumn] == MapReferences.MapUnits.NO_TRANSITABLE) ): 
				 # [hard] no transitable -> transitable
				 # hard no transitable -> no transitable
				expandedMap[l][k] = origMapMat[l - iniMapRow][k - iniMapColumn]
	
	
	return expandedMap




static func ObtainSorroundedSnake(snakeMatrix:Array, snakeHeadPos:Array)->Array:
	assert(snakeMatrix != null)
	assert(snakeMatrix != [])
	
	var newSnakeMatrix:Array = []
	var snakeN:int = snakeMatrix.size()
	var snakeM:int = snakeMatrix[0].size()
	
	var newSnakeN:int = max(snakeN + 2, 2 + MIN_EXTRA_ROW_COUNT)
	var newSnakeM:int = max(snakeM + 2, 2 + MIN_EXTRA_COLUMN_COUNT)
	
	# new matrix full of empty cells
	newSnakeMatrix = MapMatrixGenerator.matrixOfSameValues(newSnakeN, newSnakeM, MapReferences.MapUnits.EMPTY)
	
	
	# caso borde si apunta abajo
	
	var auxElement:int
	
	# add prev snake matrix to this one
	for i in range(1, snakeN + 1):
		for j in range(1, snakeM + 1):
			auxElement = snakeMatrix[i - 1][j - 1]
			if(auxElement == MapReferences.MapUnits.NO_TRANSITABLE): # coment these lines to disable no transitable transformation
				auxElement = MapReferences.MapUnits.SPAWN_FLOOR      # spawn floor won't allow object spawning to avoid possible deaths
			newSnakeMatrix[i][j] = auxElement
	
	# transform internal empty cells into transitable ones
	
	if(newSnakeN > snakeN + 2 or newSnakeM > snakeM + 2):
		for i in range(1, newSnakeN - 1):
			for j in range(1, newSnakeM - 1):
				if(newSnakeMatrix[i][j] == MapReferences.MapUnits.EMPTY):
					newSnakeMatrix[i][j] = MapReferences.MapUnits.SPAWN_FLOOR
	
	# add hard no transitable borders to snake
	for i in range(newSnakeN):
		for j in range(newSnakeM):
			if(i == 0 or i == newSnakeN - 1 or j == 0 or j == newSnakeM - 1):
				newSnakeMatrix[i][j] = MapReferences.MapUnits.HARD_NO_TRANSITABLE
	
	return newSnakeMatrix


static func obtainMapMatrixWithSnakeMapEntrance(mapMatrixWithSnake:Array)->Array:
	assert(mapMatrixWithSnake != null)
	assert(mapMatrixWithSnake != [])
	
	if(TARGET_MAP_ENTRANCE_CELLS <= 0):
		return mapMatrixWithSnake
	
	var snakeHeadPosAndDir:Array = findSnakeHeadPosAndDir(mapMatrixWithSnake)
	var n:int = mapMatrixWithSnake.size()
	var m:int = mapMatrixWithSnake[0].size()
	var headDir:int = snakeHeadPosAndDir[2]
	var headPosition:Array = [snakeHeadPosAndDir[0], snakeHeadPosAndDir[1]]
	
	var replacedCellCount:int = 0
	match headDir:
		MapReferences.MapUnits.RIGHT_SNAKE_HEAD:
			var auxRow:int = headPosition[0]
			for j1 in range(headPosition[1] + 1, m):
				if(!MapReferences.isSpawnRelatedCell(mapMatrixWithSnake[auxRow][j1])):
					mapMatrixWithSnake[auxRow][j1] = MapReferences.MapUnits.MAP_ENTRANCE
					replacedCellCount += 1
					if(replacedCellCount == TARGET_MAP_ENTRANCE_CELLS):
						break
		MapReferences.MapUnits.LEFT_SNAKE_HEAD:
			var auxRow:int = headPosition[0]
			for j1 in range(headPosition[1] - 1, -1, -1):
				if(!MapReferences.isSpawnRelatedCell(mapMatrixWithSnake[auxRow][j1])):
					mapMatrixWithSnake[auxRow][j1] = MapReferences.MapUnits.MAP_ENTRANCE
					replacedCellCount += 1
					if(replacedCellCount == TARGET_MAP_ENTRANCE_CELLS):
						break
		MapReferences.MapUnits.UP_SNAKE_HEAD:
			var auxColumn:int = headPosition[1]
			for i1 in range(headPosition[0] - 1, -1, -1):
				if(!MapReferences.isSpawnRelatedCell(mapMatrixWithSnake[i1][auxColumn])):
					mapMatrixWithSnake[i1][auxColumn] = MapReferences.MapUnits.MAP_ENTRANCE
					replacedCellCount += 1
					if(replacedCellCount == TARGET_MAP_ENTRANCE_CELLS):
						break
		MapReferences.MapUnits.DOWN_SNAKE_HEAD:
			var auxColumn:int = headPosition[1]
			for i1 in range(headPosition[0] + 1, n):
				if(!MapReferences.isSpawnRelatedCell(mapMatrixWithSnake[i1][auxColumn])):
					mapMatrixWithSnake[i1][auxColumn] = MapReferences.MapUnits.MAP_ENTRANCE
					replacedCellCount += 1
					if(replacedCellCount == TARGET_MAP_ENTRANCE_CELLS):
						break
	
	return mapMatrixWithSnake



static func obtainMapMatrixWithObstacles(mapMatrixWithSnake:Array, currentTransitableCells:int, obstacleCellPercentage:float)->Array:
	return obtainMapMatrixWithTransitableOverriding(mapMatrixWithSnake, currentTransitableCells, obstacleCellPercentage, MapReferences.MapUnits.OBSTACLE)


static func obtainMapMatrixWithFood(mapMatrixWithSnake:Array, currentTransitableCells:int, obstacleCellPercentage:float)->Array:
	return obtainMapMatrixWithTransitableOverriding(mapMatrixWithSnake, currentTransitableCells, obstacleCellPercentage, MapReferences.MapUnits.FOOD)


# returns [newMapMatrix, transitableCellCount]
static func obtainMapMatrixWithTransitableOverriding(mapMatrixWithSnake:Array, currentTransitableCells:int, cellOverridePercentage:float, newCellType:int)->Array:
	assert(mapMatrixWithSnake != null)
	assert(mapMatrixWithSnake != [])
	assert(cellOverridePercentage >= 0 and cellOverridePercentage <= 0.8) # 0.8 at maximum to avoid extra processing
	assert(MapReferences.MapUnits.values().has(newCellType)) # is a valid enum type
	
	var n:int = mapMatrixWithSnake.size()
	var m:int = mapMatrixWithSnake[0].size()
	
	var i:int = 0 
	var j:int = 0
	var lastI:int = MapReferences.INVALID_INDEX
	var lastJ:int = MapReferences.INVALID_INDEX
	var cellOverrideCount:int = currentTransitableCells * cellOverridePercentage
	# [0,0] will be an empty or no transitable cell
	for i1 in range(cellOverrideCount):
		while(mapMatrixWithSnake[i][j] != MapReferences.MapUnits.TRANSITABLE or i == lastI or j == lastJ):
			i = 1 + (randi() % (n - 2)) # [1, n - 2] range since borders are surely no transitables or empty cells
			j = 1 + (randi() % (m - 2)) # [1, m - 2] range since borders are surely no transitables or empty cells
		mapMatrixWithSnake[i][j] = newCellType # it reached a transitable cell -> replace it
		lastI = i
		lastJ = j
	
	return [mapMatrixWithSnake, currentTransitableCells - cellOverrideCount]

static func obtainCurrentTransitableCellCount(mapMatrix:Array)->int:
	assert(mapMatrix != null)
	assert(mapMatrix != [])
	
	var n:int = mapMatrix.size()
	var m:int = mapMatrix[0].size()
	
	var currentTransitableCells:int = 0
	for i in range(n):
		for j in range(m):
			currentTransitableCells += int(mapMatrix[i][j] == MapReferences.MapUnits.TRANSITABLE)
	
	return currentTransitableCells


# eraseChance -> [0,1]
static func deleteSomeNoTransitableCells(mapMatrix:Array, eraseChance:float)->Array:
	assert(mapMatrix != null)
	assert(mapMatrix != [])
	
	var auxMapMatrix:Array = mapMatrix.duplicate(true) # deatach from original ref
	var n:int = auxMapMatrix.size()
	var m:int = auxMapMatrix[0].size()
	eraseChance = eraseChance * 100 # will use percentage now
	var auxElement:int
	for i in range(1, n - 2):
		for j in range(1, m - 2):
			auxElement = auxMapMatrix[i][j]
			if(auxElement == MapReferences.MapUnits.NO_TRANSITABLE and auxElement == auxMapMatrix[i][j + 2] and \
			   auxElement != auxMapMatrix[i][j + 1] and auxElement != auxMapMatrix[i][j - 1]): # prevents no transitable gruops deletion
				if(randi() % 101 <= eraseChance):
					auxMapMatrix[i][j] = MapReferences.MapUnits.TRANSITABLE
			if(auxElement == MapReferences.MapUnits.NO_TRANSITABLE and auxElement == auxMapMatrix[i + 2][j] and \
			   auxElement != auxMapMatrix[i + 1][j] and auxElement != auxMapMatrix[i - 1][j]): # prevents no transitable gruops deletion
				if(randi() % 101 <= eraseChance):
					auxMapMatrix[i][j] = MapReferences.MapUnits.TRANSITABLE
	
	return auxMapMatrix


#if( newSnakeMatrix[i][j] == MapReferences.MapUnits.EMPTY and
#				(((j < newSnakeM - 1) and MapReferences.isSnakeCell(newSnakeMatrix[i][j + 1])) or  \
#				((j > 0) and MapReferences.isSnakeCell(newSnakeMatrix[i][j - 1])) or \
#				((i < newSnakeN - 1) and MapReferences.isSnakeCell(newSnakeMatrix[i + 1][j])) or  \
#				((i > 0) and MapReferences.isSnakeCell(newSnakeMatrix[i - 1][j])) or \
#				((i < newSnakeN - 1) and (j > 0) and MapReferences.isSnakeCell(newSnakeMatrix[i + 1][j - 1])) or  \
#				((i < newSnakeN - 1) and (j < newSnakeM - 1) and MapReferences.isSnakeCell(newSnakeMatrix[i + 1][j + 1])) or \
#				((i > 0) and (j > 0) and MapReferences.isSnakeCell(newSnakeMatrix[i - 1][j - 1])) or  \
#				((i > 0) and (j < newSnakeM - 1) and MapReferences.isSnakeCell(newSnakeMatrix[i - 1][j + 1])) ) ):
#
#				newSnakeMatrix[i][j] = 0 # adjacent cell is snake (including diagonal ones)
#

