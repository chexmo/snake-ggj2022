extends Node

const MapMatrixGenerator = preload("res://Map/MapMatrix/MapMatrixBlockGroupGenerator.gd")
const MapMatrixPrinter = preload("res://Map/MapMatrix/MapMatrixPrinter.gd")
const BaseMapMatrixGenerator = preload("res://Map/MapMatrix/BaseMapMatrixGenerator.gd")

func _ready():
	print( MapMatrixPrinter.matrix2String( [[0,0,0,0,0],
											[0,2,2,4,0],
											[0,2,0,1,0],
											[0,3,1,1,0],
											[0,0,0,0,0]]
											))
												
												
	
	var mat:Array = BaseMapMatrixGenerator.getBaseMapMatrix([[1,0,1],
															 [0,1,1],
															 [1,0,1]],
																10, 
																10, 
																[[2,2,2,2,0],
																[2,0,0,2,0],
																[2,0,0,5,0],
																[2,0,0,0,0],
																[2,0,0,0,0],
																[2,0,0,0,0],
																[2,0,0,0,0],
																[2,0,0,0,0],
																[3,0,0,0,0]]
																 )
	
	print(mat)
	MapMatrixPrinter.savePrettyMatrixInFile(mat)
	print(MapMatrixPrinter.matrix2String(mat))
#
	get_tree().quit()


