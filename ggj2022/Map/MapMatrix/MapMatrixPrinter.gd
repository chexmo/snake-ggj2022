extends Node

const SAVE_PATH = "user://saved_matrix.dat"

const TO_STRING_PREFIX = ""
const TO_STRING_ROW_ELEMENT_SEPARATOR = "" # ", "
const TO_STRING_OPENING = "" # "{"
const TO_STRING_CLOSING_INTERNAL = "\n" # "},\n"
const TO_STRING_CLOSING_EXTERNAL = "" # "};"
const TO_STRING_ELEMENTS = ["█", " ", "░", "V", "►", "◄", "▲", "▼", "-", "S", "H", "s", "O", "F"]

# references for string elements in MapReferences.gd



static func matrix2String(mat:Array)->String:
	var n: int = mat.size()
	var m: int = mat[0].size()
	var auxStr: String = TO_STRING_PREFIX
	for i in range(n):
		auxStr += TO_STRING_OPENING
		for j in range(m - 1):
			auxStr += element2String(mat[i][j]) + TO_STRING_ROW_ELEMENT_SEPARATOR
		auxStr += element2String(mat[i][m - 1])
		auxStr += TO_STRING_CLOSING_INTERNAL
	auxStr += TO_STRING_CLOSING_EXTERNAL
	
	return auxStr


static func element2String(element:int)->String:
	assert(element >= 0 and element <= TO_STRING_ELEMENTS.size() - 1)
	
	return TO_STRING_ELEMENTS[element]



static func printMatrix(mat:Array)->void:
	var n: int = mat.size()
	var m: int = mat[0].size()
	var auxStr: String = ""
	for i in range(n):
		for j in range(m):
			auxStr += str(mat[i][j]) + " "
		auxStr += "\n"
	
	print(auxStr)

static func savePrettyMatrixInFile(mat:Array)->void:
	saveStringInFile(matrix2String(mat))

static func saveStringInFile(content:String)->void:
	var file = File.new()
	file.open(SAVE_PATH, File.WRITE)
	file.store_string(content)
	file.close()
