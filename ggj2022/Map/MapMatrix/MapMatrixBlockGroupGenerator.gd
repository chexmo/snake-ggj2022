extends Node2D


const BLOCK_SIZE_H = 3
const BLOCK_SIZE_V = 3




# Pre: n > 0, m > 0, shapeMatrix != null, shapeMatrix no vacio, shape matrix con estructura de matriz [[]]
# Returns -> una matriz de ciudad con siguiendo la forma de shapeMatrix.
#			Cada 1 en la shapeMatrix representa un grupo de celdas libres y el tamaño de cada grupo de celdas libres
#			viene dado en los parametros n y m -> los grupos de cuadras son de tamaño n filas y m columnas (n*m cuadras)
#			la forma de shape matrix va a ser rotada 90 grados in game
static func getBlockGroupsWithShape(shapeMatrix: Array, n: int, m: int)->Array:
	var blockGroup: Array = _getBlockGroup(n,m)
	var auxMat:Array = _tensorMultiplyMatrices(shapeMatrix, blockGroup)
	
	
	#-----juntar filas----
	
	var extRowIntersecCount: int = shapeMatrix.size() - 1
	var rowCount: int = auxMat.size()
	
	var blockGroupRowCount: int = blockGroup.size()
	
	
	var removedRow: Array
	var currentIdx: int
	for i in range(1, extRowIntersecCount + 1):
		currentIdx = rowCount - 1 - blockGroupRowCount * i
		removedRow = auxMat[currentIdx]
		auxMat.remove(currentIdx) # saca la fila repetida
		auxMat[currentIdx] = _applyOrToArraysDim1(auxMat[currentIdx], removedRow)
		#i -= 1
	#-----juntar filas----
	
	
	#-----juntar columnas----
	var columnCount: int = auxMat[0].size()
	rowCount = auxMat.size()
	
	var auxMat2: Array = []
	auxMat2.resize(rowCount)
	
	
	
	var blockGroupColumnCount: int = blockGroup[0].size()
	
	var extColumnIntersecCount: int = shapeMatrix[0].size() - 1
	
	var j: int
	var l: int
	
	
	for i in range(rowCount):
		j = 0
		l = 0 # para recorrer las columnas de la nueva matriz en forma paralela
		auxMat2[i] = []
		auxMat2[i].resize(columnCount - extColumnIntersecCount) # las intersecciones se van a juntar por lo que son columnas de menos
		while (j < columnCount):
			if( (j + 1) % blockGroupColumnCount == 0 && j < columnCount - 1): # la ultima columna no es un caso de juntar columnas
				# unir columnas
				auxMat2[i][l] = auxMat[i][j] | auxMat[i][j + 1]
				j += 1
			else:
				auxMat2[i][l] = auxMat[i][j]
			
			j += 1
			l += 1
	
	
	#-----juntar columnas----
	
	auxMat2 = _sorroundNoRoad(auxMat2)
	
	return auxMat2


static func _sorroundNoRoad(mat: Array)->Array:
	var auxMat: Array
	
	var n: int = mat.size() + 2
	var m: int = mat[0].size() + 2
	
	auxMat = []
	auxMat.resize(n)
	auxMat[0] = _arrayOfSameValues(m, MapReferences.MapUnits.HARD_NO_TRANSITABLE) 
	auxMat[n - 1] = _arrayOfSameValues(m, MapReferences.MapUnits.HARD_NO_TRANSITABLE) 
	for i in range(1, n - 1):
		auxMat[i] = []
		auxMat[i].resize(m)
		auxMat[i][0] = MapReferences.MapUnits.HARD_NO_TRANSITABLE
		auxMat[i][m - 1] = MapReferences.MapUnits.HARD_NO_TRANSITABLE
		for j in range(1, m - 1):
			auxMat[i][j] = mat[i - 1][j - 1]
		
	
	
	return auxMat


static func _arrayOfSameValues(count:int, value: int)->Array:
	var res: Array = []
	res.resize(count)
	for i in range(count):
		res[i] = value
	
	return res

static func matrixOfSameValues(n:int, m:int, value:int)->Array:
	var mat: Array = []
	mat.resize(n)
	for i in range(n):
		mat[i] = _arrayOfSameValues(m, value)
	
	return mat



#Pre: arr1 y arr2 tienen la misma dimension
static func _applyOrToArraysDim1(arr1:Array, arr2:Array)->Array:
	
	var n: int = arr1.size()
	var res: Array = []
	res.resize(n)
	
	for i in range(n):
		res[i] = arr1[i] | arr2[i]
	
	return res


# returns -> un grupo de nxm cuadras
static func _getBlockGroup(n: int, m: int)->Array:
	var auxMat: Array = _tensorMultiplyMatrices(_getMatrixFullOf1(n,m), _getBlock3x3())
	
	var auxN: int = auxMat.size()
	var auxM: int = auxMat[0].size()
	
	var n2: int = n * (BLOCK_SIZE_V - 1) + 1 # el extremo final no se repite
	var m2: int = m * (BLOCK_SIZE_H - 1) + 1 # el extremo final no se repite
	
	return _getNonRepeatedMatrix(auxMat, n, m, auxN, auxM, n2, m2)


static func _getNonRepeatedMatrix(origMat: Array, groupCountV: int, groupCountH: int, origN: int, origM: int, goalN: int, goalM: int)->Array:
	var auxMat2 = []
	auxMat2.resize(goalN)
	
	for i in range(groupCountV):
		for l in range(BLOCK_SIZE_V - 1):
			var i2 = i * (BLOCK_SIZE_V - 1) + l
			
			auxMat2[i2] = []
			auxMat2[i2].resize(goalM)
			
			
			auxMat2[i2] = _getNonRepeatedRow(origMat, i2, i*BLOCK_SIZE_V + l, goalM, groupCountH, origM)
		
	
	auxMat2[goalN - 1] = _getNonRepeatedRow(origMat, goalN - 1, origN - 1, goalM, groupCountH, origM)
	
	return auxMat2



static func _getNonRepeatedRow(origMatrix:Array, currRow: int, currRowOrigMat: int, fixedM: int, blockCountH: int, origMatM: int)->Array:
	var auxMat2: Array
	auxMat2 = []
	auxMat2.resize(fixedM)
	for j in range(blockCountH):
		for k in range(BLOCK_SIZE_H - 1): # el -1 elimina el repetido
			auxMat2[j*(BLOCK_SIZE_H - 1) + k] = origMatrix[currRowOrigMat][j*BLOCK_SIZE_H + k]
		auxMat2[fixedM - 1] = origMatrix[currRowOrigMat][origMatM - 1]
	
	return auxMat2


# returns -> una manzana de 
static func _getBlock3x3()->Array:
	return [[1, 1, 1],
			[1, 0, 1],
			[1, 1, 1]]


static func _getMatrixFullOf1(n: int, m: int)->Array:
	var res = []
	res.resize(n)
	for i in range(n):
		res[i] = []
		res[i].resize(m)
		for j in range(m):
			res[i][j] = 1
	
	return res


# Pre: - mat1 != null y mat2 != null.  mat1 y mat2 son arreglos de arreglos (matrices)
#      - n1, n2, m1, m2 > 0 -> matrices validas
#
# Returns -> una matriz resultado de la multiplicacion de dimension (n1*n2)*(m1*m2)
static func _tensorMultiplyMatrices(mat1:Array, mat2:Array)->Array:
	var res: Array
	var n1 : int = mat1.size()
	var m1 : int = mat1[0].size() 
	var n2 : int = mat2.size()
	var m2 : int = mat2[0].size()
	res.resize(n1 * n2)
	for i in range(n1 * n2):
		res[i] = []
		res[i].resize(m1 * m2)
	for i1 in range(n1):
		for j1 in range(m1):
			for i2 in range(n2):
				for j2 in range(m2):
					res[i1 * n2 + i2][j1 * m2 + j2] = mat1[i1][j1] * mat2[i2][j2]
					#res[i1 * n2 + i2][j1 * m2 + j2] = mat1[i1][j1].multiplyTo(mat2[i2][j2])
	
	
	return res






