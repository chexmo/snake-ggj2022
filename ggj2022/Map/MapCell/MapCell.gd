extends Node


onready var spriteHappy = $SpriteHappy
onready var spriteEvil = $SpriteEvil
onready var collision = $Area2D/Collision


onready var currentSprite:Sprite = $SpriteHappy

var inHappyMap:bool = true

enum CellSprites{
	NO_DIR = 17,
	RIGHT = 1,
	LEFT = 11,
	UP = 10,
	DOWN = 0,
	RIGHT_LEFT = 2,
	RIGHT_UP = 13,
	RIGHT_UP_VARIANT = 18,
	RIGHT_DOWN = 3,
	RIGHT_DOWN_VARIANT = 8,
	LEFT_UP = 14,
	LEFT_UP_VARIANT = 19,
	LEFT_DOWN = 4,
	LEFT_DOWN_VARIANT = 9,
	UP_DOWN = 12,
	RLU = 16,
	RLD = 6,
	RDU = 15,
	LUD = 5,
	ALL_DIRS = 7
}




func setInHappyMap(cond:bool)->void:
	inHappyMap = cond
	alternateCellStyle()


func _updateSprite(transitableRight:bool, transitableLeft:bool, transitableUp:bool, transitableDown:bool)->void:
	
	if(transitableRight and transitableLeft and transitableUp and transitableDown):
		currentSprite.frame = CellSprites.ALL_DIRS
	elif(transitableRight and transitableLeft and transitableUp):
		currentSprite.frame = CellSprites.DOWN
	elif(transitableRight and transitableLeft and transitableDown):
		currentSprite.frame = CellSprites.UP
	elif(transitableRight and transitableUp and transitableDown):
		currentSprite.frame = CellSprites.LEFT
	elif(transitableLeft and transitableUp and transitableDown):
		currentSprite.frame = CellSprites.RIGHT
	elif(transitableRight and transitableLeft):
		currentSprite.frame = CellSprites.UP_DOWN
	elif(transitableRight and transitableUp):
		if(randi()%2 == 0):
			currentSprite.frame = CellSprites.LEFT_DOWN
		else:
			currentSprite.frame = CellSprites.LEFT_DOWN_VARIANT
	elif(transitableRight and transitableDown):
		if(randi()%2 == 0):
			currentSprite.frame = CellSprites.LEFT_UP
		else:
			currentSprite.frame = CellSprites.LEFT_UP_VARIANT
	elif(transitableLeft and transitableUp):
		if(randi()%2 == 0):
			currentSprite.frame = CellSprites.RIGHT_DOWN
		else:
			currentSprite.frame = CellSprites.RIGHT_DOWN_VARIANT
	elif(transitableLeft and transitableDown):
		if(randi()%2 == 0):
			currentSprite.frame = CellSprites.RIGHT_UP
		else:
			currentSprite.frame = CellSprites.RIGHT_UP_VARIANT
	elif(transitableUp and transitableDown):
		currentSprite.frame = CellSprites.RIGHT_LEFT
	elif(transitableRight):
		currentSprite.frame = CellSprites.LUD
	elif(transitableLeft):
		currentSprite.frame = CellSprites.RDU
	elif(transitableDown):
		currentSprite.frame = CellSprites.RLU
	elif(transitableUp):
		currentSprite.frame = CellSprites.RLD
	else:
		currentSprite.frame = CellSprites.NO_DIR


func _updateCollisions(cellIsTransitable:bool)->void:
	collision.disabled = cellIsTransitable


func setup(cellIsTransitable:bool, transitableRight:bool, transitableLeft:bool, transitableUp:bool, transitableDown:bool)->void:
	_updateSprite(transitableRight, transitableLeft, transitableUp, transitableDown)
	_updateCollisions(cellIsTransitable)




func alternateCellStyle():
	if(inHappyMap):
		spriteEvil.visible = false
		currentSprite = spriteHappy
		currentSprite.visible = true
	else:
		spriteHappy.visible = false
		currentSprite = spriteEvil
		currentSprite.visible = true
		



